package test.flowTest;

import java.util.concurrent.Flow;
import java.util.stream.Stream;

/**
 * Created by anel on 18/10/10.
 */

public class StreamPublisherTest {

    public static void main(String[] args) {
        new StreamPublisher<>(() -> Stream.of(1, 2, 3, 4, 5, 6))
                .subscribe(new Flow.Subscriber<>() {
                    @Override
                    public void onSubscribe(Flow.Subscription subscription) {
                        System.out.println("onSubscribe");
                        subscription.request(1);
                    }

                    @Override
                    public void onNext(Integer item) {
                        System.out.println(item);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        System.out.println("Error: " + throwable);
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Completed");
                    }
                });
    }

}
