package test.flowTest;

import java.util.Iterator;
import java.util.concurrent.Flow;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Created by anel on 18/10/10.
 */

public class StreamPublisher<T> implements Flow.Publisher<T> {
    private final Supplier<Stream<T>> streamSupplier;

    public StreamPublisher(Supplier<Stream<T>> streamSupplier) {
        this.streamSupplier = streamSupplier;
    }

    @Override
    public void subscribe(Flow.Subscriber<? super T> subscriber) {

        StreamSubscription subscription = new StreamSubscription(subscriber);
        subscriber.onSubscribe(subscription);
        subscription.doOnSubscribed();

        /*try {
            Stream<T> stream = streamSupplier.get();
            stream.forEach(subscriber::onNext);
            subscriber.onComplete();
        } catch (Throwable e) {
            subscriber.onError(e);
        }*/
    }


    private class StreamSubscription implements Flow.Subscription {
        private final Flow.Subscriber<? super T> subscriber;
        private final Iterator<? extends T> iterator;
        private final AtomicBoolean isTerminated = new AtomicBoolean(false);
        private final AtomicReference<Throwable> error = new AtomicReference<>();

        StreamSubscription(Flow.Subscriber<? super T> subscriber) {
            this.subscriber = subscriber;
            Iterator<? extends T> iterator = null;

            try {
                iterator = streamSupplier.get().iterator();
            } catch (Throwable e) {
                error.set(e);
            }

            this.iterator = iterator;
        }

        @Override
        public void request(long n) {
            if (n <= 0 && !terminate()) {
                subscriber.onError(new IllegalArgumentException("negative subscription request"));
                return;
            }

            for (long l = n; l > 0 && iterator.hasNext() && !isTerminated(); l--) {
                try {
                    subscriber.onNext(iterator.next());
                } catch (Throwable e) {
                    if (!terminate()) {
                        subscriber.onError(e);
                    }
                }
            }

            if (!iterator.hasNext() && !terminate()) {
                subscriber.onComplete();
            }
        }

        @Override
        public void cancel() {
            terminate();
        }

        void doOnSubscribed() {
            Throwable throwable = error.get();
            if (throwable != null && !terminate()) {
                subscriber.onError(throwable);
            }
        }

        private boolean terminate() {
            return isTerminated.getAndSet(true);
        }

        private boolean isTerminated() {
            return isTerminated.get();
        }
    }
}
