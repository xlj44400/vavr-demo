package test;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author anel
 * @date 2019-02-13.
 */

public class RandomTest {

    public static <T> Comparator<T> shuffle() {
        final Map<Object, UUID> uniqueIds = new IdentityHashMap<>();
        return Comparator.comparing(e -> uniqueIds.computeIfAbsent(e, k -> UUID.randomUUID()));
    }

    @Test
    public void test(){
        List<String> list=Arrays.asList("hello", "now", "shuffle", "this", "!");
        list = list.stream().sorted(shuffle()).collect(Collectors.toList());
        System.out.println(list);
    }
}
