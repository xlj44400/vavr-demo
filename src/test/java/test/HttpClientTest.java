package test;

import org.junit.Test;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

/**
 * Created by anel on 18/9/27.
 */

public class HttpClientTest {
    public CompletableFuture<String> get(String url){
        var client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(URI.create(url))
                .build();

        return client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body);
    }

    @Test
    public void test(){
       var result = get("http://baidu.com");
       result.thenAccept(System.out::println).join();
    }

}
