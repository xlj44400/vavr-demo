package test.forkjoinTest;

public interface Calculator {
    long sumUp(long[] numbers);
}
