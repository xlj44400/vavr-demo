package test.methodReference;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.*;

import static test.methodReference.Numbers.findNumbers;

/**
 * @author anel
 * @date 2019-01-24.
 */

public class MethodReferenceTest {

    //https://www.codementor.io/eh3rrera/using-java-8-method-reference-du10866vx
    // consumer
    Consumer<String> c = s -> System.out.println(s);

    Consumer<String> c1 = System.out::println;

    @Test
    public void test(){
        List<Integer> list = Arrays.asList(12,5,45,18,33,24,40);

        // Using an anonymous class
        findNumbers(list, new BiPredicate<Integer, Integer>() {
            public boolean test(Integer i1, Integer i2) {
                return Numbers.isMoreThanFifty(i1, i2);
            }
        });

        // Using a lambda expression
        findNumbers(list, (i1, i2) -> Numbers.isMoreThanFifty(i1, i2));

        // Using a method reference
        findNumbers(list, Numbers::isMoreThanFifty).forEach(System.out::println);
    }

    @Test
    public void functionTest(){
        List<Shipment> l = new ArrayList<Shipment>();

        // Using an anonymous class
        calculateOnShipments(l, new Function<Shipment, Double>() {
            public Double apply(Shipment s) { // The object
                return s.calculateWeight(); // The method
            }
        });

        // Using a lambda expression
        calculateOnShipments(l, s -> s.calculateWeight());

        // Using a method reference
        calculateOnShipments(l, Shipment::calculateWeight);

        Supplier<Double> s = Shipment::getWeight;
    }

    public List<Double> calculateOnShipments(
            List<Shipment> l, Function<Shipment, Double> f) {
        List<Double> results = new ArrayList<>();
        for(Shipment s : l) {
            results.add(f.apply(s));
        }
        return results;
    }

    @Test
    public void triFunctionTest(){

        //using an anonymous class
        TriFunction<Sum, String, String, Integer> anon =
                new TriFunction<Sum, String, String, Integer>() {
                    @Override
                    public Integer apply(Sum s, String arg1, String arg2) {
                        return s.doSum(arg1, arg2);
                    }
                };
        System.out.println(anon.apply(new Sum(), "1", "4"));

        // using a lambda expression:

        TriFunction<Sum, String, String, Integer> lambda =
                (Sum s, String arg1, String arg2) -> s.doSum(arg1, arg2);
        System.out.println(lambda.apply(new Sum(), "1", "4"));

        TriFunction<Sum, String, String, Integer> lambda1 = Sum::doSum;

        System.out.println(lambda1.apply(new Sum(),"1","4"));
    }

    public void execute(Car car, Consumer<Car> c) {
        c.accept(car);
    }

    @Test
    public void consumerTest(){
        final Mechanic mechanic = new Mechanic();
        Car car = new Car();

        // Using an anonymous class
        execute(car, new Consumer<Car>() {
            public void accept(Car c) {
                mechanic.fix(c);
            }
        });

        // Using a lambda expression
        execute(car, c -> mechanic.fix(c));

        // Using a method reference
        execute(car, mechanic::fix);
    }

    @Test
    public void supplierTest(){
        // Using an anonymous class
        Supplier<List<String>> s = new Supplier() {
            public List<String> get() {
                return new ArrayList<String>();
            }
        };
        List<String> l = s.get();

        // Using a lambda expression
        Supplier<List<String>> s1 = () -> new ArrayList<String>();
        List<String> l1 = s1.get();

        // Using a method reference
        Supplier<List<String>> s2 = ArrayList<String>::new;
        List<String> l2 = s2.get();

    }

    @Test
    public void test3(){
        // Using an anonymous class
        Function<String, Integer> f =
                new Function<String, Integer>() {
                    public Integer apply(String s) {
                        return new Integer(s);
                    }
                };
        Integer i = f.apply("100");

        // Using a lambda expression
        Function<String, Integer> f1 = a -> new Integer(a);
        Integer i1 = f1.apply("100");

        // Using a method reference
        Function<String, Integer> f2 = Integer::new;
        Integer i2 = f2.apply("100");
    }

    @Test
    public void test4(){
        // Using a anonymous class
        BiFunction<String, String, Locale> f = new BiFunction<String, String, Locale>() {
            public Locale apply(String lang, String country) {
                return new Locale(lang, country);
            }
        };
        Locale loc = f.apply("en","UK");

        // Using a lambda expression
        BiFunction<String, String, Locale> f1 = (lang, country) -> new Locale(lang, country);
        Locale loc1 = f1.apply("en","UK");

        // Using a method reference
        BiFunction<String, String, Locale> f2 = Locale::new;
        Locale loc2 = f2.apply("en","UK");
    }
}

class Numbers {
    public static boolean isMoreThanFifty(int n1, int n2) {
        return (n1 + n2) > 50;
    }
    public static List<Integer> findNumbers(
            List<Integer> l, BiPredicate<Integer, Integer> p) {
        List<Integer> newList = new ArrayList<>();
        for(Integer i : l) {
            if(p.test(i, i + 10)) {
                newList.add(i);
            }
        }
        return newList;
    }
}

class Shipment {
    public double calculateWeight() {
        double weight = 0;
        // Calculate weight
        return weight;
    }

    public static double getWeight() {
        double weight = 0;
        // Calculate weight
        return weight;
    }
}

class Sum {
    Integer doSum(String s1, String s2) {
        return Integer.parseInt(s1) + Integer.parseInt(s2);
    }
}

interface TriFunction<T, U, V, R> {
    R apply(T t, U u, V v);
}

class Car {
    private int id;
    private String color;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
class Mechanic {
    public void fix(Car c) {
        System.out.println("Fixing car " + c.getId());
    }
}