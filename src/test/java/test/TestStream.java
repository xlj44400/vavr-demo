package test;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

/**
 * Created by anel on 18/10/22.
 */

public class TestStream {

    private static List<Integer> apply(List<Integer> l, Integer e) {
        l.add(e);
        return l;
    }

    @Test
    public void test01() {
        List<Integer> datas = asList(1, 2, 3, 1, 5, 6, 7);

        datas.stream().filter(e->e>4).collect(Collectors.toList());

        List<Integer> result = datas.stream().dropWhile(e -> e < 2).collect(Collectors.toList());

        System.out.println(result);


        List<Integer> numbers = datas.stream()
                .reduce(new ArrayList<Integer>(),
                        TestStream::apply, (List<Integer> l1, List<Integer> l2) -> {
                            l1.addAll(l2);
                            return l1;
                        });

        System.out.println(numbers);
    }

    @Test
    public void testGroupBy() {
        Map<String, List<String>> dishTags = new HashMap<>();
        dishTags.put("pork", asList("greasy", "salty"));
        dishTags.put("beef", asList("salty", "roasted"));
        dishTags.put("chicken", asList("fried", "crisp"));
        dishTags.put("french fries", asList("greasy", "fried"));
        dishTags.put("rice", asList("light", "natural"));
        dishTags.put("season fruit", asList("fresh", "natural"));
        dishTags.put("pizza", asList("tasty", "salty"));
        dishTags.put("prawns", asList("tasty", "roasted"));
        dishTags.put("salmon", asList("delicious", "fresh"));
    }

    @Test
    public void testCollect(){
        List<Integer> a = Stream.of(1,2,3,4,5,6,7,8).collect(Collectors.toList());
        //a.removeIf(e->e>3);
        //System.out.println(a);

        var b = a.iterator();
        a.forEach(e->{
            System.out.println(b.next());
        });

        IntStream.rangeClosed(1, 10).forEach( System.out::println);
    }


    @Test
    public void test(){
        List<ParamTest> list = Arrays.asList(new ParamTest("A1",1),
                new ParamTest("A3",3),new ParamTest("A2",2),new ParamTest("A4",4));
        List<String> stringList = list.stream()
                .sorted((Comparator.comparingInt(ParamTest::getIndex)))
                .map(ParamTest::getName)
                .collect(Collectors.toList());
        System.out.println(stringList);
    }

    public class ParamTest{
        private String name;
        private int index;

        public ParamTest(String name, int index) {
            this.name = name;
            this.index = index;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }
    }
}
