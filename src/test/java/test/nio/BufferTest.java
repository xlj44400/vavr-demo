package test.nio;

import org.junit.Test;

import java.nio.Buffer;
import java.nio.CharBuffer;

/**
 * @author anel
 * @date 2019-03-18.
 */

public class BufferTest {
    @Test
    public void bufferTest(){
        // 创建Buffer
        CharBuffer buff = CharBuffer.allocate(8); // ①指定大小，只有ByteBuffer才提供了创建直接Buffer allocateDirect()成本高效率高，适用于长生存期的Buffer
        System.out.println("capacity: " + buff.capacity());
        System.out.println("limit: " + buff.limit());
        System.out.println("position: " + buff.position());
        // 放入元素
        buff.put('a');
        buff.put('b');
        buff.put('c'); // ②
        System.out.println("加入三个元素后，position = " + buff.position());
        // 调用flip()方法
        buff.flip(); // ③将 limit 指向 position,position指向0
        System.out.println("执行flip()后，limit = " + buff.limit());
        System.out.println("position = " + buff.position());
        // 取出第一个元素
        System.out.println("第一个元素(position=0)：" + buff.get()); // ④
        System.out.println("取出一个元素后，position = " + buff.position());
        // 调用clear方法
        buff.clear(); // ⑤ limit指向最后既capacity,position指向0
        System.out.println("执行clear()后，limit = " + buff.limit());
        System.out.println("执行clear()后，position = " + buff.position());
        System.out.println("执行clear()后，缓冲区内容并没有被清除：" + "第三个元素为：" + buff.get(2)); // ⑥
        System.out.println("执行绝对读取后，position = " + buff.position());
    }

    @Test
    public void bufferTest01(){
        CharBuffer buff = CharBuffer.allocate(8);
        printLog(buff);
        // 放入元素
        buff.put('a');
        buff.put('b');
        buff.put('c'); // ②
        printLog(buff);
        buff.flip();
        System.out.println("第一个元素(position=0)：" + buff.get(3));
    }

    private void printLog(Buffer buff){
        System.out.println("capacity: " + buff.capacity());
        System.out.println("limit: " + buff.limit());
        System.out.println("position: " + buff.position());
    }
}
