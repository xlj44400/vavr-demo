package test.nio;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author anel
 * @date 2019-03-18.
 */

public class RandomFileChannelTest {

   public static void main(String[] args){
       try {
           RandomAccessFile file = new RandomAccessFile("/Users/user/Desktop/book1", "rw");
           FileChannel channel = file.getChannel();
           ByteBuffer buffer = ByteBuffer.allocate((int)file.length());

           int bytesRead = channel.read(buffer);
           while (bytesRead != -1) {
               System.out.println("Read " + bytesRead);
               buffer.flip();
               while(buffer.hasRemaining()){
                   System.out.print((char) buffer.get());
               }
               buffer.clear();
               bytesRead = channel.read(buffer);
           }
           channel.write(buffer);
           file.close();
       } catch (IOException e) {
           e.printStackTrace();
       }

   }
}
