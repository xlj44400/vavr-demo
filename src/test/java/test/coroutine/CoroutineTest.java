/*
package test.coroutine;

import de.esoco.coroutine.Continuation;
import de.esoco.coroutine.Coroutine;
import de.esoco.coroutine.CoroutineScope;
import de.esoco.coroutine.step.Delay;
import de.esoco.lib.datatype.Range;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static de.esoco.coroutine.Coroutine.first;
import static de.esoco.coroutine.CoroutineScope.launch;
import static de.esoco.coroutine.step.CodeExecution.apply;
import static de.esoco.coroutine.step.CodeExecution.run;
import static de.esoco.coroutine.step.Iteration.collectEach;
import static org.junit.Assert.assertEquals;

*/
/**
 * Created by anel on 2018-11-11.
 *//*


public class CoroutineTest {

    @Test
    public void test(){
        Coroutine<?, ?> crunchNumbers =
                first(run(() -> Range.from(1).to(10).forEach(e->{
                    System.out.println(Thread.currentThread().getName());
                })));

        launch(scope -> {
            // start a million coroutines
            Delay.sleep(100);
            for (int i = 0; i < 10000; i++) {
                crunchNumbers.runAsync(scope);
            }
        });
    }

    @Test
    public void t1(){
        Coroutine coroutine = first(run(()->{

        }));
    }

    @Test
    public void testIteration()
    {
        Coroutine<String, List<String>> cr =
                Coroutine.first(apply((String s) -> Arrays.asList(s.split(","))))
                        .then(collectEach(apply((String s) ->
                                s.toUpperCase())));

        launch(
                scope ->
                {
                    Continuation<?> ca = cr.runAsync(scope, "a,b,c,d");
                    Continuation<?> cb = cr.runBlocking(scope, "a,b,c,d");

                    assertEquals(Arrays.asList("A", "B", "C", "D"), ca.getResult());
                    assertEquals(Arrays.asList("A", "B", "C", "D"), cb.getResult());
                });
    }

}
*/
