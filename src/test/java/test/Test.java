package test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.lambda.snamp.Utils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Created by anel on 18/10/12.
 */

public class Test {

    @org.junit.Test
    public void test(){
        Arrays.asList("111http://localhost/", "https://github.com")
                .stream()
                .map(e->{
                    try {
                        return  new URL(e);
                    } catch (MalformedURLException e1) {
                        e1.printStackTrace();
                        return null;
                    }
                })
                .collect(Collectors.toList()).forEach(System.out::println);
    }

    @org.junit.Test
    public void read() throws IOException {
        String testFilePath = "/Users/user/Desktop/startit/postlimit.txt";
        File testFile = new File(testFilePath);
        List<String> lines = Files.readLines(testFile, Charsets.UTF_8);
        List<Map<Integer,String>> list = new ArrayList<>();
        for (String line : lines) {
            var a = line.split(";");
            Map map = new HashMap();
            map.put(Integer.valueOf(a[0]),a[1].trim());
            list.add(map);
        }
        System.out.println(JSON.toJSON(list));
    }

    @org.junit.Test
    public void tt(){
        List<Integer> indexs = Stream.of(1,2,3).collect(toList());
        if(true){
            indexs = Stream.of(1,2,3,4,5,6,7,8).collect(toList());
        }
        indexs.removeIf(e->Arrays.asList(1,2,3,5,6).stream().anyMatch(f-> f == e));
        System.out.println(indexs);
    }
}
