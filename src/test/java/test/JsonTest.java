package test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anel on 18/10/22.
 */

public class JsonTest {

    @Test
    public void test(){
        String str = "[\"http://192.168.0.24:8091/group1/M00/05/10/wKgAGFg-hZaARlDHAAA6s59jjUA473.jpg\",\"http://192.168.0.24:8091/group1/M00/05/10/wKgAGFg-hZeAdeVrAAA6s59jjUA999.jpg\",\"http://192.168.0.24:8091/group1/M00/05/10/wKgAGFg-hZiANi3xAAA6s59jjUA882.jpg\"]";

        JSONArray json = JSON.parseArray(str);
        for(int i = 0;i<json.size();i++){
            System.out.println(json.get(i));
        }
    }

    private static final Map<String,String> MAP_FILTERS = new HashMap<>();

    static {
        MAP_FILTERS.put("<","&lt;");
        MAP_FILTERS.put(">","&gt;");
        MAP_FILTERS.put("\"","&quot;");
        MAP_FILTERS.put("'","&apos;");
    }

    public String replaceSpecialChar(String str){
        if(str.contains("&")){ // 这个必须放到第一个替换
            str = str.replaceAll("&","&amp;");
        }
        for(String key : MAP_FILTERS.keySet()){
            if(str.contains(key)){
                str = str.replaceAll(key,MAP_FILTERS.get(key));
            }
        }
        return str;
    }

    @Test
    public void t(){
        String a = "a&B  &&& \"";
        System.out.println(replaceSpecialChar(a));

        List<String> b = Arrays.asList("a","b","c");
        System.out.println(b.toString());
    }
}
