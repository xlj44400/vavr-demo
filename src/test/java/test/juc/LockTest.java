package test.juc;

/**
 * @author anel
 * @date 2019-05-25.
 */

public class LockTest {

    private int value =0;

    private void add(){
        synchronized (new Object()){
            value++;
        }
    }

    private void inc(){
        synchronized (new Object()){
            value--;
        }
    }

    public int getValue() {
        return value;
    }

    public static void main(String[] args) throws InterruptedException {
        LockTest lockTest = new LockTest();

        for(int b=0;b<10;b++){
            new Thread(()->{
                for(int a=0;a<1000;a++){
                    lockTest.add();
                }
            }).start();

            new Thread(()->{
                for(int a=0;a<1000;a++){
                    lockTest.inc();
                }
            }).start();
        }
        Thread.sleep(5000);
        System.out.println(lockTest.getValue());
    }
}
