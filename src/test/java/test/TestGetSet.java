package test;

import com.lambda.snamp.SettersJava11;
import org.junit.Test;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.invoke.MethodHandles;
import java.util.Date;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.lambda.snamp.GetSetDemo.reflectGetter;
import static com.lambda.snamp.GetSetDemo.reflectSetter;

/**
 * Created by anel on 18/10/12.
 */

public class TestGetSet {

    @Test
    public void demo() throws ReflectiveOperationException {
        final Date d = new Date();
        final BiConsumer<Date, Long> timeSetter = reflectSetter(MethodHandles.lookup(), Date.class.getDeclaredMethod("setTime", long.class));
        timeSetter.accept(d, 42L); //the same as d.setTime(42L);
        final Function<Date, Long> timeGetter = reflectGetter(MethodHandles.lookup(), Date.class.getDeclaredMethod("getTime"));
        System.out.println(timeGetter.apply(d)); //the same as d.getTime()
    }

    @Test
    public void jdk11() throws Exception {


        final Date d = new Date();
        final Function<Date, Long> timeGetter = reflectGetter(MethodHandles.lookup(), Date.class.getDeclaredMethod("getTime"));
        System.out.println(timeGetter.apply(d)); //the same as d.getTime()

        final BeanInfo beanInfo = Introspector.getBeanInfo(Date.class);
        final Function<String, PropertyDescriptor> property = name -> Stream.of(beanInfo.getPropertyDescriptors())
                .filter(p -> name.equals(p.getName()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Not found: " + name));

         //PropertyDescriptor nameProperty = property.apply("name");
         PropertyDescriptor valueProperty = property.apply("time");

        final var lookup = MethodHandles.lookup();
        final BiConsumer valueSetter = SettersJava11.createSetter(lookup,
                lookup.unreflect(valueProperty.getWriteMethod()), valueProperty.getPropertyType());

        valueSetter.accept(d,timeGetter.apply(d));


    }
}
