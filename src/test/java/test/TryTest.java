package test;

import io.vavr.control.Try;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;

import java.util.List;

/**
 * Created by anel on 18/10/8.
 */

public class TryTest {

    @Test
    public void test(){
        Try<Integer> result = divide(1,1);
        result.onSuccess(e-> System.out.println(e)).onFailure(Throwable::printStackTrace);
        //System.out.println(result.isFailure());
    }

    public Try<Integer> divide(Integer dividend, Integer divisor) {
        return Try.of(() -> dividend / divisor);
    }

    @Test
    public void toVavr(){
        List<String> list = List.of("1","2","3");
        var a = io.vavr.collection.List.ofAll(list);
        a.forEach(e -> {
            System.out.println(e);
        });

        Try.failure(new Exception()).failed();
    }
}
