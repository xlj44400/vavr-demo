package test.rxTest;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * @author anel
 * @date 2019-05-21.
 */

public class RxHideTest {

    private static BehaviorSubject<Integer> a = BehaviorSubject.createDefault(5);

    static {
        Observable.just(1,2,3)
                .map(v->v*2).subscribe(state->{
            a.onNext(state);
        });
    }

    private static BehaviorSubject<Integer> listenToViewState(){
        return a;
    }

    public static void main(String[] args) {

    }
}
