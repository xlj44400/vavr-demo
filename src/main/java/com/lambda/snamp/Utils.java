package com.lambda.snamp;

import com.google.common.base.Joiner;
import com.lambda.snamp.ArrayUtils;
import com.lambda.snamp.SpecialUse;

import java.io.PrintStream;
import java.lang.invoke.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.Spliterator;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by anel on 18/10/12.
 */

public final class Utils {
    @FunctionalInterface
    private interface SilentInvoker extends Function<Callable<?>, Object> {
        MethodType SIGNATURE = MethodType.methodType(Object.class, Callable.class);//signature after type erasure

        @SpecialUse({SpecialUse.Case.REFLECTION, SpecialUse.Case.JVM})
        <V> V invoke(final Callable<V> callable);

        @Override
        default Object apply(final Callable<?> callable) {
            return invoke(callable);
        }

        @SpecialUse({SpecialUse.Case.REFLECTION, SpecialUse.Case.JVM})
        static <V> V call(final Callable<V> callable) throws Exception {
            return callable.call();
        }
    }

    private static final SilentInvoker SILENT_INVOKER;

    static {
        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        try {
            final CallSite site = LambdaMetafactory.metafactory(lookup,
                    "invoke",
                    MethodType.methodType(SilentInvoker.class),
                    SilentInvoker.SIGNATURE,
                    lookup.findStatic(SilentInvoker.class, "call", SilentInvoker.SIGNATURE),
                    SilentInvoker.SIGNATURE);
            SILENT_INVOKER = (SilentInvoker) site.getTarget().invokeExact();
        } catch (final Throwable e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    private Utils(){
        throw new InstantiationError();
    }

    private static String getStackTrace(StackTraceElement[] stackTrace) {
        if (stackTrace.length > 0)
            stackTrace = ArrayUtils.remove(stackTrace, 0);
        return Joiner.on(System.lineSeparator()).join(stackTrace);
    }

    /**
     * Prints the stack trace. Used for debugging purposes.
     */
    public static void printStackTrace(final PrintStream output){
        final StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for(int i = 2; i < stackTrace.length; i++)
            output.println(stackTrace[i]);

    }


    /**
     * Used to initialize static fields in interfaces
     * when initialization code may throw exception.
     *
     * @param <T>  the type parameter
     * @param initializer the initializer
     * @return The value returned from initializer.
     * @throws ExceptionInInitializerError the exception in initializer error
     */
    public static <T> T staticInit(final Callable<T> initializer){
        return callAndWrapException(initializer, ExceptionInInitializerError::new);
    }

    public static <T, E extends Throwable> T callAndWrapException(final Callable<? extends T> task, final Function<? super Exception, ? extends E> wrapper) throws E{
        try {
            return task.call();
        } catch (final Exception e) {
            throw wrapper.apply(e);
        }
    }

    public static <I, E extends Throwable> E wrapException(final I input,
                                                           final Exception toWrap,
                                                           final Function<? super I, ? extends E> wrapper) {
        final E newException = wrapper.apply(input);
        newException.initCause(toWrap);
        return newException;
    }

    private static Supplier<?> reflectGetter(final MethodHandles.Lookup lookup,
                                             final Object owner,
                                             final MethodHandle getter) throws ReflectiveOperationException {
        final MethodType invokedType = owner == null ?
                MethodType.methodType(Supplier.class) :
                MethodType.methodType(Supplier.class, owner.getClass());

        try {
            final CallSite site = LambdaMetafactory.metafactory(lookup, "get",
                    invokedType,
                    MethodType.methodType(Object.class),
                    getter,
                    MethodType.methodType(getter.type().returnType()));
            return (Supplier<?>) (owner == null ? site.getTarget().invoke() : site.getTarget().invoke(owner));
        } catch (final LambdaConversionException e){
            throw new ReflectiveOperationException(e);
        } catch (final Throwable e){
            throw new InvocationTargetException(e);
        }
    }

    public static Supplier<?> reflectGetter(final MethodHandles.Lookup lookup,
                                            final Object owner,
                                            final Method getter) throws ReflectiveOperationException {
        return reflectGetter(lookup, owner, lookup.unreflect(getter));
    }

    private static Consumer reflectSetter(final MethodHandles.Lookup lookup,
                                          final Object owner,
                                          final MethodHandle setter) throws ReflectiveOperationException {
        final MethodType invokedType;
        final MethodType instantiatedMethodType;
        if(owner == null){
            invokedType = MethodType.methodType(Consumer.class);
            instantiatedMethodType = MethodType.methodType(void.class, setter.type().parameterType(0));
        }
        else {
            invokedType = MethodType.methodType(Consumer.class, owner.getClass());
            instantiatedMethodType = MethodType.methodType(void.class, setter.type().parameterType(1));//zero index points to 'this' reference
        }
        try {
            final CallSite site = LambdaMetafactory.metafactory(lookup,
                    "accept",
                    invokedType,
                    MethodType.methodType(void.class, Object.class),
                    setter,
                    instantiatedMethodType);
            return (Consumer) (owner == null ? site.getTarget().invoke() : site.getTarget().invoke(owner));
        } catch (final LambdaConversionException e) {
            throw new ReflectiveOperationException(e);
        } catch (final Throwable e) {
            throw new InvocationTargetException(e);
        }
    }

    public static Consumer reflectSetter(final MethodHandles.Lookup lookup,
                                         final Object owner,
                                         final Method setter) throws ReflectiveOperationException {
        return reflectSetter(lookup, owner, lookup.unreflect(setter));
    }

    public static <T> void parallelForEach(final Spliterator<T> spliterator,
                                           final Consumer<? super T> action,
                                           final ExecutorService threadPool) {
        final class ParallelForEachTasks extends LinkedList<Callable<Void>> implements Callable<Object> {
            private static final long serialVersionUID = -2010068532370568252L;

            @Override
            public Object call() throws InterruptedException {
                return threadPool.invokeAll(this);
            }

            private void add(final Spliterator<T> s) {
                add(() -> {
                    s.forEachRemaining(action);
                    return null;
                });
            }
        }

        final ParallelForEachTasks tasks = new ParallelForEachTasks();
        {
            Spliterator<T> subset = spliterator.trySplit();
            for (int i = 0; i < Runtime.getRuntime().availableProcessors() && subset != null; i++, subset = spliterator.trySplit())
                tasks.add(subset);
        }
        tasks.add(spliterator);
        callUnchecked(tasks);
    }

    /**
     * Calls code with checked exception as a code without checked exception.
     * <p>
     *     This method should be used instead of wrapping some code into try-catch block with ignored exception.
     *     Don't use this method to hide checked exception that can be actually happened at runtime in some conditions.
     * @param callable Portion of code to execute.
     * @param <V> Type of result.
     * @return An object returned by portion of code.
     */
    public static <V> V callUnchecked(final Callable<V> callable) {
        return SILENT_INVOKER.invoke(callable);
    }

    /**
     * Closes many resources in guaranteed manner.
     * @param resources A set of resources to close.
     * @throws Exception One or more resource throw exception when closing.
     */
    public static void closeAll(final AutoCloseable... resources) throws Exception {
        Exception e = null;
        for (final AutoCloseable closeable : resources)
            if (closeable != null)
                try {
                    closeable.close();
                } catch (final Exception inner) {
                    if (e == null)
                        e = inner;
                    else
                        e.addSuppressed(inner);
                }
        if (e != null)
            throw e;
    }
}
